FROM docker.io/ruby:2.3 AS serve

WORKDIR /serve

COPY . .
COPY _config-docker.yml _config.yml 

RUN bundle install

EXPOSE 4000

ENTRYPOINT [ "bundle", "exec", "jekyll", "serve", "-H", "0.0.0.0", "--future", "--incremental"]

FROM docker.io/nginx:latest
COPY _site /usr/share/nginx/html
RUN rm -f /etc/nginx/conf.d/default.conf
COPY nginx/default.conf /etc/nginx/conf.d/default.conf