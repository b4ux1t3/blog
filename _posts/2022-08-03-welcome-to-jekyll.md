---
layout: post
title:  "Welcome to Jekyll - Redux"
categories: jekyll hello ci-cd
---

### Note: I have since moved this blog over to my new website, which you're reading this on now, so none of the links are going to work anymore. Still, I hope it proves useful to someone!

Getting started with Jekyll is surprisingly straightforward. Given it just generates a static site based on markdown, it more or less obviates the need for something like Ghost, which I currently use to run [my current dev blog](https://blog.bauxite.tech).

This post is, more or less, a placeholder. I don't have any profound tips or tricks, and I'm not planning on telling you [how to learn something](https://blog.bauxite.tech/front-end-to-full-stack-part1/).

Soon, maybe.

In the mean time, let's talk about the process (which is still ongoing) of making this blog.

## Cloning the Repo

This was the first, simplest step. Just click on the nice little plus button at the top of any GitLab page, and then select "New project/repository", "Create from template", and find "Pages/Jekyll, then click "Use Template". _Simplicity incarnate_. From there, you're given a (more or less) functioning GitLab Pages site. This is where the simplicity ends.

## Customizing the configuration.

There are a few things you really need to change when you first clone the site. This won't act as a comprehensive list, but these are what I ended up changing:

* `title` - This is what shows up at the bottom of the page (unless you have a custom theme) and in the tab's title.
* `description` - this shows up in Google (or Bing or DuckDuckGo, or Yandex or. . .) searches by way of `meta` tags, and shows up at the bottom of every page (again, unless you have a custom theme).
* `email` - I got rid of this, I don't need spam bots emailing me. You'll also need to get rid of a portion of one of the template files, which we'll talk about shortly.
* `baseurl` and `url` - Er, we'll talk abouit this.
* * Your socials - you can figure this part out.

### Broken Styles

If you're checking out this blog's repository (over at [GitLab](https://gitlab.com/b4ux1t3/blog)), you'll notice a huge list of very small commits right at the beginning of the repository's life. That was me hand-jamming pseudo-randomly chosesn paths into the `_config.yml` file. 

The short version of the problem is that GitLab[^github] Pages is not a traditional file HTTP server. I mean, it is, but it also isn't. When you set up a GitLab Pages, er, page, you are essentially supplying GitLab with a list of static files to serve[^1], and GitLab serves them from a directory structure that starts with the _name of your project_, from a server host named `yourusername.gitlab.io`. 

To take an example, if your username is `b4ux1t3` and your project is namged `blog`, GitLab Pages will grab everything out of your `public` directory at the root of your repository and serve it from a host named `b4ux1t3,gitlab.io`, at a path named `blog`, for a final URI[^2] of `https://b4ux1t3.gitlab.io/blog`.

The only special case here is your GitLab Pages "home page", which you can populate by having a pages project with a project name that _exactly_ matches the GitLab Pages URL for your GitLab Pages domain, in this case `b4ux1t3.gitlab.io`.

Yes, that was the short version. I establish all of that as a baseline to describe the problem. This problem will be familiar to anyone who, say, wants to deploy an Angular application[^3] to GitLab Pages: You can't rely on there being anything at your root path, unless you specifically account for that in your home page. Of course, [I do that](https://b4ux1t3.gitlab.io). But I don't want to share styles between my blog and my other pages, not even my home page. I have projects that would completely break if I did that.

So, to finish this _way too long_ section with the solution, the new `_config.yml` looks like this:

{% highlight yaml %}

. . .
baseurl: "/blog" 
url: "https://b4ux1t3.gitlab.io/"
. . .

{% endhighlight %}

Your `baseurl` should be the name of your project (in this case, `blog`), and your url should be your GitLab Pages domain (including the scheme!). This will make it so that the default HTML file that's generated properly points to the resulting CSS file. The template looks like this:

{% highlight html %}

  <link rel="stylesheet" href="{{ "/css/main.css" | prepend: site.baseurl }}">

{% endhighlight %}

. . .and the actual HTML delivered to your users looks like this:

{% highlight html %}

  <link rel="stylesheet" href="/blog/css/main.css">

{% endhighlight %}

I could probably have avoided the stack of tiny commits if I'd just ~~squashed them all to hide my crimes~~ read the documentation more thoroughly, but, meh, who wants to take time to do that?

## Nixing the Email

If you look in the `_includes/footer.html` file, you can find where the email from teh configuration is referenced. It looks like this:

{% highlight html %}

<li><a href="mailto:{{ site.email }}">{{ site.email }}</a></li>

{% endhighlight %}

Kill it if you don't want it to look broken.

## Conclusion

All in all, it took longer to write up this short blog post than it did to actually get Jekyll up and running on GitLab Pages. I did a couple other things, like getting rid of my `master` branch in favor of `main`, but they're hardly important.

I'm liking Jekyll so far. It really does what it says on the tin, and I didn't have to spend a day trying to configure a reverse proxy to get it working, like I did with Ghost. Mind you, I'm a big fan of Ghost, and if I were doing anything other than simply putting up blog posts, I would likely stick to it. My entire (personal) web presence is static and, as such, I can save a lot of time and money by just using GitLab Pages.

Feel free to chew me out on Twitter for doing things wrong!

____

[^github]: GitHub Pages works more or less the same way.
[^1]: out of a special directory called `public`, always. There is more setup required for this, but we'll leave that for [GitLab to explain](https://docs.gitlab.com/ee/user/project/pages/)
[^2]: or URL, I can never actually remember the difference, despite literally two decades of doing this crap
[^3]: That's a topic for another blog post, where I go over how I put a [sample Angular app](https://gitlab.com/b4ux1t3/recipe-book) on [GitLab Pages](https://b4ux1t3.gitlab.io/recipe-book)!
