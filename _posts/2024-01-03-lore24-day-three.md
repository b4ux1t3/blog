---
layout: post
title:  "#Lore24 Day Three"
categories: lore24 ttrpg worldbuilding ugdoq
---

Today's prompt: Location (Room)

> The wall is plastered with scraps of parchment with hastily-scrawled notes, and bits of string attached with small pins. It is obvious from the sheer number of slips that Ugdoq has been working on this for years. His quest to find the source of his freedom took him all around \<INSERT WORLD NAME HERE>.
>
> His bed is meticulously made, despite being a few rags rolled up as pillows and a hide cloak as a blanket.
>
> A few coins lay scattered on a side table, next to a pile of parchment scraps and an inkwell and quill.

10d6 of the average denomination coin are on the table.

The inkwell, when cleaned, is stamped with the insignia of a rather powerful baron.

Players succeeding a reasonably-hard perception, will, or presence (_depending on the game system_) check will find and enciphered scroll attached to the bottom of the bed's frame.
