---
layout: post
title:  "Making a Game in 30 Days: Day 4"
categories: game-dev
---

This post might feel a little different from the rest, because I am going to "liveblog" the thought- and work processes for this work session. I haven't worked out exactly how I want to add levels, enemies, etc. to the game, so I want to lay out what I _do_ want and see where that takes us. 

I don't expect to actually get much code written this time, so I might end up making a "Day Four-and-a-Half". In the end, I've probably spent much less than an actual day (as in, 8 hours) on this project. I need to really think about how I structure my time for this project! 

## Building a world

I want worlds to be generated from configuration files. I want to be able to define where my enemies sit, what kind of enemies they are, and what sorts of obstacles exist in the world all in a plain text format. I want them to be loaded into a generic `World` class that the `GameController` can turn into a playable world on the canvas. I _do not_ want to define an interface for `World` and have various implementations (`FirstWorld`, `WorldTwo`, whatever). That would be the easy way to do things, but the point of this isn't to get things done easily, it's to do them (more or less) _correctly_. Or something, is there really an actual point to this project?

This is the first bit of complexity. I have resource types, including a blob type. But I could just tack on another resource type, the `WorldResource` type. I intend to have everything bundled up by webpack, so I don't think I need to worry about making this thing transmissible over the network. . .but then, why not? That could, potentially, drop the size of my page load pretty significantly. Meh, maybe for V2. ;)

So, I have two options, more or less:

1. Figure out how to import JSON files directly into TypeScript (while maintaining type safety!), or
2. serve the json files as fetchable resources

I'm really tempted to do the second, but adding that much complexity might be a pain in the butt. So, you know what? We're just going to go with option 1. I think this should be pretty straightforward, according to a [cursory search](https://stackoverflow.com/questions/49996456/importing-json-file-in-typescript).

## Panning for gold. . .or through a world?

I have already done a little bit of work on defining what I am calling a `Boundary`. It's a class that stores a position, a width and a height. I've also adapted a [`QuadTree`](https://gitlab.com/b4ux1t3/ugdoq-escapes/-/merge_requests/3/diffs#diff-content-26cb110ea06cf9e1ee9b7db4cd9d1b83b719ec88) class I wrote a little while ago as a test to fit this project's style.

The goal here is that each `World` will have a `QuadTree`, in which it places its `Characters`. I've decided that I'm going to use the `Character` interface to represent basically anything that can be placed into the world. That means that the player and any NPCs or enemies will end up being `Characters`, but it _also_ means that scenery, like rocks, houses and rivers, also count as Characters.

Wait wait wait, you ask, why the heck are you using a quad tree? Isn't that a bit overkill for something like this?

Yes, yes it is. Anyway. . .

The `World` will also have a `Camera`, which is essentially just a `Boundary` that the `GameController` can control, using it to establish what `Characters` exist within the Canvas's view area. Since we'll have access to an arbitrarily large area, we can pan the `Camera` around just by moving it around in the coordinate space and querying the quad tree! That lets us only sender what we want to render without doing _too_ much extra math.

This concept of screen space versus world space is abstracted away really well in engines like Unity, so I'm going to try to accomplish a similar level of abstractness. 

I'll need to find a good camera algorithm to follow the player around. When they approach the edge, I'd like there to be a smooth transition along the playable area.

The last piece of the puzzle, then, is the `World`'s play area, which is effectively a boundary that dictates where `Characters` can move within the `QuadTree`. Since I plan on having the game be a sort of 30-degree side scrolling isometric view, this means that I can have the top part of th screen just be set dressing (housing the HUD, for example) while moving left and right along the `World`'s play area. I can even, maybe, let there be flying NPCs, but that might end up being a little confusing in this perspective. We'll see!

The best part of doing what I'm doing is that, although my game is effectively only going to have one "dimension" of camera movement, (with two dimensions of player and NPC movement), there's nothing really stopping me from expanding this into a "proper" 2-D world with arbitrarily-size height as well as width. 

___

That's all I've got for now. Like I said, not a whole lot of code in this blog, since I didn't actually implement much. However, now that I've typed all of this out, I think I have a plan of action! Tomorrow (er, the next workday, whenever that is), I'll start working on the JSON format for the worlds, and figure out how to load them as proper `World` objects.