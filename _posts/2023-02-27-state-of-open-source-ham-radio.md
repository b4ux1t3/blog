---
layout: post
title:  "The State of Open Source in Ham Radio Software"
categories: ham-radio amateur-radio open-source
---

I am a ham radio operator. In fact, I am, seemingly, the stereotypical ham radio operator: I'm roughly middle-aged (Geez, when did that happen?), white, and male. I've only had my "ticket" (my amateur radio license) since October, but I already feel like I've identified many. . .let's call them "issues" with ham radio as a hobby.

I'm not going to be talking about any of the various and sundry social justice issues which pervade most communities made up largely of older white dudes. There are many more people who are more qualified, both in and out of the ham community, than I am to speak on those topics, and I highly recommend you give those voices the attention they deserve. 

No, instead, I'm going to be talking about the state of open source software in ham radio. Or, rather, I'm going to talk about the _lack of_ open source software in what is, on the surface, supposed to be a hacker's wet dream of a hobby.

In the first part of this (two-part[^1]) blog post, I'm going to talk a little about what ham radio means to me, and what drew me to the hobby. I'll also talk about some of the problems with existing open source SDR software. In the second part, I'm going to talk about what I see as a rather large problem with the hobby as a whole: the lack of a hacker's ethos.

## Ham Radio is Cool.

I have been playing with the idea of becoming a ham for the better part of two decades. I never had the money or time to invest, however, so I avoided it. I _also_ saw it as a "Good Old Boys" social club, and I have no interest in that scene whatsoever. 

{% include note.html content="A lot of the hobby still IS a glorified social club. That said, a lot of it also isn't." %}

There is an undeniable geeky draw to ham radio as a hobby: the thought of soldering a circuit, plugging it in to a massive, homebrewed radio mast, and being able to send and receive signals to and from not just our planet, but also to and from _space_ is, as I said earlier, a hacker's wet dream. I always thought of hams, whatever else they are, as hackers[^2] at their core.

I am not a ham radio elitist, though. Every radio I own is one that I bought. I've considered building one, and even have plans to. But, I am not going to bemoan people buying a $25 Baofeng so that they can get into the hobby. I'm not going to complain that new hams just buy antennas instead of building them. That's because I'm not, as some would say, a Sad Ham™️.

When I first started listening in to ham radio transmissions back in July of last year, I Bought myself an [RTL-SDR](https://www.rtl-sdr.com/) dongle. These things are _awesome_. They cost less than fifty bucks, _including_ an antenna and tripod and decently-long coaxial cable. These things work because someone hacked a TV tuner. You heard that right: These things are literally just a broadcast TV tuner with some fancy circuit wizardry. I was _blown away_ that I could have an SDR for under 500 bucks, much less under 50. Listening to ham radio (and non-ham radio! you can use this thing to listen to air bands, satellites, or pretty much anything) has never been cheaper or easier, and it's due in large part to the hacker mindset.

And so, when I started thinking about getting my ticket a couple months later, I had grand dreams of using open source hardware and software to talk to people all around the world.

## The cracks started appearing

Let's start looking at some of the software I used to listen in to ham radio traffic on my cool, sub-$50 SDR. One of the most-used pieces of software on Windows is [SDR#](https://airspy.com/download/), a proprietary program by the company AirSpy. It's a pretty good piece of software, all things considered. It even has a C# SDK you can use to develop plugins for it. Pretty neat!

But, I'm somewhat of an open-source nut. I wanted something that I could call mine, that I could be sure I could keep using if AirSPy decided to lock down their product. Not that things like that ever happen, of course. 😉 SDR# comes from a time when C# and .NET were very much proprietary tools, even if you were, generally, free to use them.

So I looked around and, lo, I found a few options for listening to my SDR with open source software. Here are three major players[^3]:

1. [GQRX](https://gqrx.dk/) - A GNU Radio-powered Qt app. 100% open source, and really robust.
2. [SDR++](https://www.sdrpp.org/) - More or less a direct answer to SDR#'s proprietary nature.
3. [CubicSDR](https://cubicsdr.com/) - A very powerful piece of software that's not just a response to another piece of software existing.

All of these pieces of software are great. Seriously, I enjoy using all of them. That said, _holy crap_, they are stuck in very old-fashioned development processes.

## Let's look in those cracks

I am not afraid of a little command line foo. I'm that stereotypical linux nerd who recompiles his kernel on a regular basis. I'm typing this blog post on an Arch Linux (BTW) laptop which I've compiled about 80% of the software for. I'm _also_ a big proponent of automation and reproducibility. Heck, a significant portion of my actual job is getting software build systems to work correctly.

I have an [open ticket](https://github.com/cjcliffe/CubicSDR/issues/959) with CubicSDR where I point out that I cannot, for the life of me, get it to build a working copy of the software from the docs. This is why I flexed my little nerd biceps earlier: I want to make it clear that _I build software from source on a daily basis_. And I can't get a working copy of CubicSDR without downloading one that they built. . .five years ago.

You heard that right: The version of CubicSDR that I run every day (It is part of the background of my [stream](https://twitch.tv/b4ux1t3) on Twitch) is from [five years ago](https://cubicsdr.com/?p=190). I hear you typing to me on [Mastodon](https://hachyderm.io/@b4ux1t3). Let me finish your toot for you: "Well, maybe it's just been abandoned and needs someone to fork it and continue development". But wait! CubicSDR is actually not abandoned, and is, indeed, in [active development](https://github.com/cjcliffe/CubicSDR).

{% include note.html content="Sharp-eyed readers might notice that there is actually a much more recent release in their [GitHub Releases](https://github.com/cjcliffe/CubicSDR/releases/tag/0.2.7). It still hasn't been added to their website, and I don't know why that is, so I don't currently use it. You should also note that it is, as of the time of this writing, from over a year ago now." %}

## A premature conclusion

It shouldn't be difficult to build software in 2023. I have the utmost respect and admiration (not to mention gratitude) for the folks behind these projects. However, I think we can do better. Hams are hackers! We need to make these thing better. For my part, I'm trying to de-funk the CUbicSDR build process, and get it as automated as possible. I plan on putting up a PR in the near-ish future to fix this.

The thing is, I haven't even started touching on actual ham radio software yet. SDR isn't just for ham radios, and, frankly, it's _great_ compared to the state of actual ham radio software.

In the next part, I'll talk about what your options are for the various types of ham radio activities, both open source and otherwise. From proprietary digital modes to battleship gray logging software with no license actually mentioned, we'll see just how much of the hacker spirit seems to exist in ham radio software.
___

[^1]: I really need to start writing some part twos.
[^2]: We're using the [unofficial official](https://people.eecs.berkeley.edu/~bh/hacker.html) term here.
[^3]: There are PLENTY of other SDR software suites out there, open source and otherwise. I'm using these three because I have the most experience with them.