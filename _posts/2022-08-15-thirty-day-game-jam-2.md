---
layout: post
title:  "Making a Game in 30 Days: Day 2"
categories: game-dev
---

Today was mostly a lot of infrastructure work, and a bit (er, a _lot_) of learning. I refactored my Vector2 class a bit, as well as added some functionality. Additionally, I worked through a [tutorial](https://www.youtube.com/watch?v=pyhb8Y9qKUI&list=PLYElE_rzEw_uryBrrzu2E626MY4zoXvx2&index=4) in order to nail down how I want to handle things like sprite movement and animation. 

## Turorial "Hell" 

I'm a firm believer in "just doing it", and getting out of what some newer developers dub "tutorial hell". The big problem with tutorials when you're a beginner is that you're not thinking for yourself. You're just copying code verbatim and not learning how to troubleshoot things. The best part of being an Experienced Developer™️ is that you don't have to take tutorials at face value. Once you've debugged a few real-world applications, figuring out why you're getting an error message when the tutorial creator didn't is as easy as looking at the documentation. Spoilers, it's usually a dependency versioning issue.

The great thing about tutorials like the one I'm following is that he's not using any libraries, so you if you have an error, it's probably because you mistyped something. He does a great job of actually laying out the code and explainiong what everything does, and doesn't use any magic. This is actually _supremely annoying_ for someone like me, who not only is more than experienced enough to understand the concepts he's teaching without code examples, but is also doign it in a different (well, different _enough_) language.

Full disclosure, this tutorial series is actually one of the big inspirations behind this "jam". I'm [working through it](https://b4ux1t3.gitlab.io/fcc-game-dev) as I work on this project and, while the techniques demonstrated in the tutorials are rather rudimentary, they're great jumping off points. You can see from [the code](https://gitlab.com/b4ux1t3/fcc-game-dev/-/tree/main/003-enemy-movement/src) for my implementation of the course projects that I'm testing out the project and code structure for my [jam game](https://gitlab.com/b4ux1t3/ugdoq-escapes/-/blob/adding-movement/src/movement/Vector.ts) on the individual sections of the [tutorial series](https://gitlab.com/b4ux1t3/fcc-game-dev/-/blob/main/003-enemy-movement/src/Vector2.ts).

Importantly, I'm almost completely ignoring the actual implementations from the videos in favor of my own, TypeScript-y goodness, like my [`Enemy` interface](https://gitlab.com/b4ux1t3/fcc-game-dev/-/blob/main/003-enemy-movement/src/enemies/Enemy.ts) and [corresponding](https://gitlab.com/b4ux1t3/fcc-game-dev/-/blob/main/003-enemy-movement/src/enemies/GhostEnemy.ts) [implementations](https://gitlab.com/b4ux1t3/fcc-game-dev/-/blob/main/003-enemy-movement/src/enemies/SawEnemy.ts). There is, unfortunately, still a lot of duplication of code, _mostly_ because there isn't a concept of _abstract_ classes in TypeScript, only interfaces. I could build a base class which the enemies inherit from. . .but then I'm violating my core tenet of "Don't ever use base classes, just interfaces" that I've developed over the past couple decades.

In the end, I'll likely come up with a bit more separated concerns once I start fleshing out the game itself.

## Vector Woes

While implementing the final enemy in this class, I came to the conclusion that I need to do vector math correctly. No more of this "morphing" the vectors in place. With two notable exceptions, all of my vector methods now return a new vector. I needed this becasue otherwise implementing the saw enemy's pathing was a nightmare of `new Vector3(this.destination.x, this.destination.y)`. I now understand why basically every game engine does exactly what I'm doing; it's just better.

![enemy movement]({{ "/assets/enemy_movement.gif" | prepend: site.url }})

Here you can see the paths that the enemies take. The way I would have had to do that previously was by caching the values of my position, multiplying that cached value by negative 1 and adding that to my destination. Now, I still have to do all those steps, with the exception of the caching step. I can just get the inverse of my position and add that to the destination directly, giving me my velocity. Then I just have to normalize and scale that velocity, and bam, I have my path.

I also added the ability to get a vector's magnitude, and the ability to get the distance between two vectors, which is just the magnitude of their difference! Now you can start to see where making the vector math a bit more ergonomic came in handy.

## Debug Tools

Debugging is the most important skill you can have as a devloper. Similarly, debugging _tools_ are some of the most important pieces of kit when you're developing software. I spent a lot of time today familiarizing myself with the canvas's drawing API with the express intention of building in debug tools to my game. You can see the tools in action in the above gif, where the saw enemy shows its path, and all of the enemies can show their "hit boxes". Kind of a moot point, since there is nothing to "hit" them, but it's good practice!

I [greally enjoy](https://gitlab.com/b4ux1t3/emulators/-/blob/main/CPUs/SixFiveOhTwo/Cpu/Debugging.cs) building [debugging tools](https://b4ux1t3.gitlab.io/processing-game/) and displays, so I really need to watch myself on scope here. I can't get caught up on polishing my dev tools when I have an actual game to polish!

___

Next on the docket is to actually get player movement and controls in. I dabbled in that a [little bit](https://gitlab.com/b4ux1t3/fcc-game-dev/-/blob/main/003-enemy-movement/src/index.ts#L39) today, and I find myself _really_ missing the tools that Unity and Godot make available handling user input.