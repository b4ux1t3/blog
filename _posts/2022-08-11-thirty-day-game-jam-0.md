---
layout: post
title:  "Making a Game in 30 Days: Day 0"
categories: game-dev
published: true
---

## Or, a terrible idea, documented

I'm going to make a game in 30 days. Thirty _individual_ days. That means that I won't necessarily be publishing a game on September 11th. I plan to slot in those days between now and the end of the year.

## Why?

I've tried to participate in a few game jams over the years, but, between work and family life, I can never seem to put aside enough time for a specific game jam. I've made some decent efforts in, for example, the [Weekly Game Jam](http://weeklygamejam.com), and even made a whole game [back in 2016](https://b4ux1t3.itch.io/ludum-dare-38-chicken-little) for a Ludum Dare. . .for which I forgot to include the Unity data package, and then promptly left it to rot for a year before remembering that I'd made it. I even tried to host my own jam in the month leading up to my son's birth last year, which went about as well as anyone who's ever had a kid thinks it went.

I also have a few "full-fledged" game concepts that are sitting unfinished and neglected on just about every computer I own. They'll get worked on eventually. Maybe.

The problem, as far as I can tell, is that I'm always just a little bit too ambitious. I decide that I'm going to do a game in two days and then I decide "I'm going to make a game in my spare time", and in boths cases the game balloons in scope and ends up getting abandoned either a day in because I have other responsibilities (game jams) or a week in because I have other responsibilities (non-game jams). In either case, the ambition is just too high: I don't have the amount of spare time that I need to make a game in two days, and I don't have the amount of spare time to make a game whose scope is indeterminate and constantly changing.

Enter: the 30-day challenge. I know I can't finish a game in two days. I also know I _won't_ finish a game if I have an indefinite deadline. Thirty days worked for [Game Maker's Toolkit](https://www.youtube.com/watch?v=0lhjLNYopHM), maybe it'll work for me!

## How?

The key will be scope management. My manager will tell you in no uncertain terms that I am _terrible_ at this. Despite programming for most of my life, I am really, really bad at figuring out how long something will take to finish. So, let's define the scope _now_:

* The game will be about a little goblin named Ugdoq. He's out of a setting I've been building for a few years for D&D. He's a nice guy.
  
![Ain't he a looker?](https://gitlab.com/b4ux1t3/ugdoq-escapes/-/raw/main/public/img/ugdoq-512x512.png)

* Ugdoq needs to find `McGuffin` in order to accomplish `some goal`.
* Ugdoq isn't very nimble, and as such won't be doing any platforming.
* Ugdoq will travel through three levels:
  * Leaving his tribe
  * Escaping the mountain
  * Finding the nearest human settlement
* You'll see Ugdoq from a 45-degree "isometric" view. Basically, a top-down shooter, probably without any shooting.

Yeah, that's all. That's the extent of the game. 

* No platforming (no fiddly controls for me to endlessly fret over)
* No 3D world (no fiddly game engine for me to endlessly fret over)
* A specific narrative that I want to communicate

## What?

The game will be written in plain HTML5 and TypeScript. No game engines, not even Phaser. Since Phaser has crappy TypeScript support _anyway_, I'm not losing out on much. I don't need an engine for something as simple as this. This isn't one of my "I'm not using _any_ libraries!" projects, though. If I find a library that does something for me, I'm going to use it. I just don't intend to use any engines.

The game will be hosted on [GitLab](https://gitlab.com/b4ux1t3/ugdoq-escapes). I have a side goal to have a "playable" version of the game every day, but that isn't a promise.

I intend to make all the game assets myself, with the exception of music. I'll be drawing all the art, recording all the sounds.

__________

That's all I've got for now. The repo for the game is live, but I've only pushed up the infrastructure bits so far. I plan to blog every day that I work on the game, and to end each day's blog post with the plan for that day, or what was accomplished that day. 

Goal for day one is to get a basic movement system up and running, and get some placeholder images in. Wish me luck!

EDIT: Day One post is [live](/blog/game-dev/2022/08/12/thirty-day-game-jam-1)