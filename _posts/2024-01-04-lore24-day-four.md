---
layout: post
title:  "#Lore24 Day Three"
categories: lore24 ttrpg worldbuilding ugdoq
---

Today's prompt: Location (Country)

# Hroth'Far (Westlands)

> Far to the west lies the land of the barbarian warlords. Not much is known about the structure of their society, as most interactions with them start and end in bloodshed.
>
>What the eastern men and dwarves do not know is that goblin-kind has traded with the barbarians for centuries, and they enjoy an amicable peace with the supposedly war-like human nation.

It begs the question: What might the easterners be doing wrong?